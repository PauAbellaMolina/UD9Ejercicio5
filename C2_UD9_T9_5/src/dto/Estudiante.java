package dto;

public class Estudiante extends Personal{
     
	//Aqu� creo el atributo nota
    private int nota;
     
    //Aqu� creo un constructor donde indicamos una nota y edad aleatorias al estudiante
    public Estudiante() {
        nota=numeroAleatorio(0,10);
        super.setEdad(numeroAleatorio(10,18));
    }
 
    //Metodo que modifica si el estudiante asiste siguiendo el 50% de probabilidades de que sea asi
    public void disponible() {
        if (numeroAleatorio(0, 100) < 50) {
            super.setAsistencia(false);  
        } else {
            super.setAsistencia(true);
        }
    }
     
    //Aqu� creo un toString para poder mostrar por consola los valores de los atributos creados
    public String toString() {
        return "Nombre estudiante: " + super.getNombre() + " - Sexo: " + super.getSexo() + " - Nota: " + nota;
    }

    //Aqu� creo los getters y los setters del atributo nota
    public int getNota() {
    	return nota;
    }
    
    public void setNota(int nota) {
    	this.nota = nota;
    }   
}