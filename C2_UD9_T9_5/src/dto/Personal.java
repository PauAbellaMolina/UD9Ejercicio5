package dto;

public abstract class Personal {
    //Aqu� creo las variables como atributos
    private String nombre;
    private char sexo;
    private int edad;
    private boolean asistencia;
     
    //Aqu� creo las variables como constantes
    private final String[] NOMBRES_H={"Sergi", "Felipe", "Dani", "Aitor", "Pasqual"}; 
    private final String[] NOMBRES_M={"Rosa", "Antonia", "Angeles", "Misericordia", "Mireia"}; 
    
	//Aqu� creamos un m�todo donde generamos un n�mero aleatorio
    public static int numeroAleatorio(int minimo, int maximo) {
    	int num=(int)Math.floor(Math.random()*(minimo-(maximo+1))+(maximo+1));
        return num;
    }

    //Aqu� creo el constructor Persona, donde determinar� el sexo dependiendo de si da 0 o 1
    public Personal() {
        //Aqu� ser� donde descubriremos si es 0 o 1
        //En el caso de que sea 0 ser� una chica, en el caso de 1 un chico
    	if (numeroAleatorio(0, 1) == 1) {
			nombre = NOMBRES_H[numeroAleatorio(0, 4)];
			sexo = 'H';
		} else {
			nombre = NOMBRES_M[numeroAleatorio(0, 4)];
			sexo = 'M';
		}
    	
    	//Llamamos al metodo disponible que setteara el atributo asistencia segun las probabilidades de los Estudiantes y Profesores de asistir
        disponible();
    }
    
    //Declaramos que las classes hijas de esta classe (Estudiante y Profesor) deben implementar el metodo disponible
    protected abstract void disponible();

	//Aqu� hago todos los setters y getters necesarios
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public String getNombre() {
        return nombre;
    }
    public char getSexo() {
        return sexo;
    }
 
    public void setSexo(char sexo) {
        this.sexo = sexo;
    }
 
    public int getEdad() {
        return edad;
    }
 
    public void setEdad(int edad) {
        this.edad = edad;
    }
 
    public boolean isAsistencia() {
        return asistencia;
    }
    
    public void setAsistencia(boolean asistencia) {
        this.asistencia = asistencia;
    }
}
