package dto;

public class Aula {
     
    //Aqu� creo los atributos
    private int id;
    private String materia;
    private Profesor profesor;
    private Estudiante[] estudiantes;
    
    //Aqu� creo la lista de materias
  	final static String[] MATERIAS_AULA = {"matematicas", "filosofia", "fisica"};
    
    //Aqu� creamos un m�todo donde generamos un n�mero aleatorio
    public static int numeroAleatorio(int minimo, int maximo){
    	return (int)Math.floor(Math.random()*(minimo-(maximo+1))+(maximo+1));
    }
    
    //Aqu� crear� un constructor donde le pondr� valor a los atributos creados anteriormente
    public Aula() {
        id=1;
        materia=MATERIAS_AULA[numeroAleatorio(0,2)];
        profesor=new Profesor();
        estudiantes=new Estudiante[25];
        //Llamamos al metodo que rellena el array anterior
        rellenarEstudiantes();
    }
     
    //Aqu� creo un m�todo que rellena el array de estudiantes con nuevos objetso estudiante
    private void rellenarEstudiantes() {
        for(int i=0;i<estudiantes.length;i++){
        	estudiantes[i]=new Estudiante();
        }
    }
    
    //Aqu� hacemos un bucle tipo for donde sumamos un contador en el caso de que el total de contadores
    //sea igual a como m�nimo la mitad de la aula
    private boolean asistenciaEstudiantes() {
        int contadorAsistenciaEstudiante = 0;

        for(int i=0;i<estudiantes.length;i++){
            if(estudiantes[i].isAsistencia()){
            	contadorAsistenciaEstudiante++;
            }
        }
        
        //Aqu� creo una condici�n donde me calcula que si el n�mero de contadores �s superior a la longitud de la lista de alumno
        // dividida entre dos (la mitad en total), que devuelva un true, de lo contrario, devolver� un false
        if (contadorAsistenciaEstudiante > (estudiantes.length/2)) {
        	return true; 
    	} else {
    		return false;
		}
    }
    
    public boolean estadoClase() {
    	//Aqu� creo una condici�n donde si el profesor no esta disponible, no se podr� dar clase
        if (!profesor.isAsistencia()) {
            System.out.println("El profesor no se encuentra disponible, hoy no hay clase");
            return false;
        }
        //Aqu� creo una condici�n donde si la materia del profesor no le pertenece al aula, no se podr� dar clase
        else if (!profesor.getMateria().equals(materia)) {
            System.out.println("La materia que ense�a el profesor no pertenece a este aula, hoy no hay clase");
            return false;
        }
        //Aqu� creo una condici�n donde si la asisterncia de clase no llega como m�nimo a la mitad, no se podr� dar clase
        else if (!asistenciaEstudiantes()) {
            System.out.println("No han asistido suficientes alumnos, hoy no hay clase");
            return false;
        }
        
        //En el caso de que ninguna de las anteriores condiciones se cumplan, se podr� dar clase
        System.out.println("Hoy hay clase");
        return true;
    }
    
    //Aqu� creo un m�todo donde muestro la cantidad de chicos y chicas que han aprobado
    public void mostrarNotas() {
        //Aqu� creo dos variables para ir sumando al cantidad de chicos y chicas
        int chicosAprobados=0;
        int chicasAprobadas=0;
        
        System.out.println("");
        System.out.println("Alumnos assistentes a classe:");
        for(int i=0;i<estudiantes.length;i++){
        	//Aqu� creo una condici�n que indica si el alumno est� aprobado
        	if(estudiantes[i].getNota() >= 5){
        		//Dependiendo de si es chico o chica, el contador correspondiente se sumar�
        		if(estudiantes[i].getSexo() == 'H'){
        			chicosAprobados++;   
    			}
        		if (estudiantes[i].getSexo() == 'M'){
        			chicasAprobadas++;
    			}
    		}
           //Aqu� mostramos la informacion de todos los alumnos de classe, hayan aprobado o no
           System.out.println(estudiantes[i].toString());
        }
         //Aqu� muestro la cantidad de chicos y chicas que han aprobado
        System.out.println("");
        System.out.println("Alumnos aprobados: ");
        System.out.println("Hay "+ chicosAprobados + " chicos y " + chicasAprobadas + " chicas que han aprobado su asignaturas");
    }
}