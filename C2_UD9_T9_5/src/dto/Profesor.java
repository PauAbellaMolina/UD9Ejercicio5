package dto;

public class Profesor extends Personal {

	//Aqu� creo el atributo materia
	private String materia;
	
	//Aqu� creo un constructor donde indicamos una edad aleatoria y una materia aleatoria de la lista de materias creada en la clase Aula
	public Profesor() {
		super.setEdad(numeroAleatorio(20, 70));
		materia = Aula.MATERIAS_AULA[numeroAleatorio(0, 2)];
	}
	
	//Metodo que modifica si el profesor esta disponible siguiendo el 20% de probabilidades de que sea asi
	public void disponible() {
		if (numeroAleatorio(0, 100) <= 20) {
			super.setAsistencia(false);
		} else {
			super.setAsistencia(true);
		}	
	}

	//Aqu� creo los getters y los setters del atributo materia	
	public String getMateria() {
		return materia;
	}
	
	public void setMateria(String materia) {
		this.materia = materia;
	}
}
