import dto.Aula;

public class claseApp {

	public static void main(String[] args) {
		//Aqu� creo un objeto llamado aula
        Aula aulaDeClase=new Aula();
         
        //Aqu� analizo si se puede dar clase en la aula y si es asi, muestra los estudiantes que asisten y los apobados
        if(aulaDeClase.estadoClase()){
        	aulaDeClase.mostrarNotas();
        }
	}
}
